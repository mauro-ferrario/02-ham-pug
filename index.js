const express = require("express");
const haml = require("hamljs");
const pug = require("pug");
const cons = require("consolidate");
const port = 3000;
const ip = "0.0.0.0";
const app = express();

var names = [];
names.push("Giovanni");
names.push("Antonio");
names.push("Maria");

app.set("views", "views");
//app.engine('.haml', cons.haml);
//app.set("view engine", "haml");
app.engine('.haml', cons.pug);
app.set("view engine", "pug");

app.use(express.static('public'))

app.get("/", function(req, res)
{
  res.render("index");
})

app.get("/test", function(req, res)
{
  res.render("test", {players: names});
})

const server = app.listen(port, ip, function()
{
  console.log("server started on "+ip+":"+port);
})